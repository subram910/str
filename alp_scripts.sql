-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bms
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bms
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bms` DEFAULT CHARACTER SET latin1 ;
USE `bms` ;

-- -----------------------------------------------------
-- Table `bms`.`user_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bms`.`user_details` (
  `ACCOUNT_NUMBER` BIGINT(20) NOT NULL,
  `ACCOUNT_BALANCE` BIGINT(20) NULL DEFAULT NULL,
  `ACCOUNT_HOLDER_NAME` VARCHAR(255) NULL DEFAULT NULL,
  `ACCOUNT_TYPE` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_NUMBER`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bms`.`education_loan_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bms`.`education_loan_details` (
  `EDUCATION_LOAN_ID` VARCHAR(255) NOT NULL,
  `COURSE_FEE` BIGINT(20) NULL DEFAULT NULL,
  `COURSE_NAME` VARCHAR(255) NULL DEFAULT NULL,
  `Father_Annual_Income` BIGINT(20) NULL DEFAULT NULL,
  `FATHER_NAME` VARCHAR(255) NULL DEFAULT NULL,
  `ID_CARD_NUMBER` BIGINT(20) NULL DEFAULT NULL,
  `EDU_LOAN_ACCOUNT_NUMBER` VARCHAR(255) NULL DEFAULT NULL,
  `EDU_LOAN_AMOUNT` BIGINT(20) NULL DEFAULT NULL,
  `EDU_LOAN_APPLY_DATE` DATE NULL DEFAULT NULL,
  `EDU_LOAN_DURATION` INT(11) NULL DEFAULT NULL,
  `ACCOUNT_NUMBER` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`EDUCATION_LOAN_ID`),
  INDEX `FK_8gclerl7ejc4w2q8t6bcp1pwj` (`ACCOUNT_NUMBER` ASC),
  CONSTRAINT `FK_8gclerl7ejc4w2q8t6bcp1pwj`
    FOREIGN KEY (`ACCOUNT_NUMBER`)
    REFERENCES `bms`.`user_details` (`ACCOUNT_NUMBER`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bms`.`home_loan_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bms`.`home_loan_details` (
  `homeLoanId` VARCHAR(255) NOT NULL,
  `ANNUAL_INCOME` BIGINT(20) NULL DEFAULT NULL,
  `COMP_NAME` VARCHAR(255) NULL DEFAULT NULL,
  `EXP_CURRENT_COMP` INT(11) NULL DEFAULT NULL,
  `DESIGNATION` VARCHAR(255) NULL DEFAULT NULL,
  `HOME_LOAN_ACCOUNT_NUMBER` VARCHAR(255) NULL DEFAULT NULL,
  `LOAN_AMOUNT` BIGINT(20) NULL DEFAULT NULL,
  `LOAN_APPLY_DATE` DATE NULL DEFAULT NULL,
  `LOAN_DURATION` INT(11) NULL DEFAULT NULL,
  `TOTAL_EXP` INT(11) NULL DEFAULT NULL,
  `HOME_ACCOUNT_NUMBER` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`homeLoanId`),
  INDEX `FK_6d6hkint7wbqkx0ap1n1iqiv2` (`HOME_ACCOUNT_NUMBER` ASC),
  CONSTRAINT `FK_6d6hkint7wbqkx0ap1n1iqiv2`
    FOREIGN KEY (`HOME_ACCOUNT_NUMBER`)
    REFERENCES `bms`.`user_details` (`ACCOUNT_NUMBER`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bms`.`tr_transaction_pk_table`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bms`.`tr_transaction_pk_table` (
  `TRANSACTION_ID` VARCHAR(255) NOT NULL,
  `VALUE` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`TRANSACTION_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `bms`.`transaction_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bms`.`transaction_details` (
  `transactionId` BIGINT(20) NOT NULL,
  `ACCOUNT_BALANCE` BIGINT(20) NULL DEFAULT NULL,
  `TRANSACTION_DESCRIPTION` VARCHAR(255) NULL DEFAULT NULL,
  `TRANSACTION_TYPE` VARCHAR(255) NULL DEFAULT NULL,
  `TR_ACCOUNT_NUMBER` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`transactionId`),
  INDEX `FK_eyxb517gvklq7r7hafy28ocll` (`TR_ACCOUNT_NUMBER` ASC),
  CONSTRAINT `FK_eyxb517gvklq7r7hafy28ocll`
    FOREIGN KEY (`TR_ACCOUNT_NUMBER`)
    REFERENCES `bms`.`user_details` (`ACCOUNT_NUMBER`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
